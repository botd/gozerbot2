# BOTLIB - Framework to program bots.
#
#

""" kick/ban a user. """

import queue
import time

from lo.shl import rlog

from bot import k

bans      = {}
cachetime = 300
timeout   = 10 

def handle_367(bot, ievent):
    rlog(0, '367', str(ievent))
    channel = ievent.arguments[1].lower()
    if not bot.cfg.server in bans or not channel in bans[bot.cfg.server]:
        return # not requested by this plugin
    bans[bot.cfg.server][channel].append(ievent.txt.split()[0])

def handle_mode(bot, ievent):
    rlog(0, 'mode', str(ievent))
    # [18 Jan 2008 13:41:29] (mode) cmnd=MODE prefix=maze!wijnand@2833335b.cc9dd561.com.hmsk postfix=#eth0-test +b *!*@je.moeder.ook arguments=[u'#eth0-test', u'+b', u'*!*@je.moeder.ook'] nick=maze user=wijnand userhost=wijnand@2833335b.cc9dd561.com.hmsk channel=#eth0-test txt= command= args=[] rest= speed=5 options={}

def get_bans(bot, channel):
    # :ironforge.sorcery.net 367 basla #eth0 *!*@71174af5.e1d1a3cf.net.hmsk eth0!eth0@62.212.76.127 1200657224
    # :ironforge.sorcery.net 367 basla #eth0 *!*@6ca5f0a3.14055a38.89.123.imsk eth0!eth0@62.212.76.127 1200238584
    # :ironforge.sorcery.net 368 basla #eth0 :End of Channel Ban List
    bot = k.fleet.by_orig(ievent.orig)
    channel = channel.lower()
    if not bot.cfg.server in bans:
        bans[bot.cfg.server] = {}
    bans[bot.cfg.server][channel] = []
    queue368 = queue.Queue()
    bot.wait.register('368', channel, queue368)
    bot.sendraw('MODE %s +b' % (channel, ))
    # wait for End of Channel Ban List
    try:
        res = queue368.get(1, timeout)
    except queue.Empty:
        return None
    return bans[bot.cfg.server][channel]

def get_bothost(bot):
    return getwho(bot, bot.nick).split('@')[-1].lower()

def handle_ban_list(bot, ievent):
    banslist = get_bans(bot, ievent.channel)
    if not banslist:
        ievent.reply('the ban list for %s is empty' % ievent.channel)
    else:
        ievent.reply('bans on %s: ' % ievent.channel, banslist, nr=True)

def handle_ban_remove(bot, ievent):
    channel = ievent.channel.lower()
    if len(ievent.args) != 1 or not ievent.args[0].isdigit():
        ievent.missing('<banlist index>')
        return
    if not bot.cfg.server in bans or not channel in bans[bot.cfg.server]:
        banslist = get_bans(bot, ievent.channel)
    else:
        banslist = bans[bot.cfg.server][channel]
        index = int(ievent.args[0])-1
        if len(banslist) <= index:
            ievent.reply('ban index out of range')
        else:
            unban = banslist[index]
            banslist.remove(unban)
            bot.sendraw('MODE %s -b %s' % (channel, unban))
            ievent.reply('unbanned %s' % (unban, ))

def handle_ban_add(bot, ievent):
    if not ievent.args:
        ievent.missing('<nick>')
        return
    if ievent.args[0].lower() == bot.nick.lower():
        ievent.reply('not going to ban myself')
        return
    userhost = getwho(bot, ievent.args[0])
    if userhost:
        host = userhost.split('@')[-1].lower()
        if host == get_bothost(bot):
            ievent.reply('not going to ban myself')
            return
        bot.sendraw('MODE %s +b *!*@%s' % (ievent.channel, host))
        ievent.reply('banned %s' % (host, ))
    else:
        ievent.reply('can not get userhost of %s' % ievent.args[0])

def handle_kickban_add(bot, ievent):
    if not ievent.args:
        ievent.missing('<nick> [<reason>]')
        return
    if ievent.args[0].lower() == bot.nick.lower():
        ievent.reply('not going to kickban myself')
        return
    userhost = getwho(bot, ievent.args[0])
    reason = len(ievent.args) > 1 and ' '.join(ievent.args[1:]) or 'Permban requested, bye'
    if userhost:
        host = userhost.split('@')[-1].lower()
        if host == get_bothost(bot):
            ievent.reply('not going to kickban myself')
            return
        bot.sendraw('MODE %s +b *!*@%s' % (ievent.channel, host))
        bot.sendraw('KICK %s %s :%s' % (ievent.channel, ievent.args[0], reason))
    else:
        ievent.reply('can not get userhost of %s' % ievent.args[0])
