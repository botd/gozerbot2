# setup.py

from setuptools import setup

def read():
    return open("README.rst", "r").read()

setup(
    name='gozerbot2',
    version='1',
    url='https://bitbucket.org/botd/gozerbot2',
    author='Bart Thate',
    author_email='bthate@dds.nl',
    description=""" GOZERBOT2 is a gozerbot clone using BOTLIB. no copyright. no LICENSE. """,
    long_description=read(),
    long_description_content_type="text/x-rst",
    license='Public Domain',
    zip_safe=True,
    install_requires=["botlib"],
    packages=["gozerbot2"],
    classifiers=['Development Status :: 3 - Alpha',
                 'License :: Public Domain',
                 'Operating System :: Unix',
                 'Programming Language :: Python',
                 'Topic :: Utilities'
                ]
)
