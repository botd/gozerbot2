# plugs/udp.py
#
#

"""
    the bot has the capability to listen for udp packets which it will use
    to /msg a given nick or channel.

    1) setup

        * do !reload udp to enable the udp plugin
        * call !udp-cfgsave to generate a config file in gozerdata/plugs/udp/config
        * edit this file .. esp. look at the udpallowednicks list
        * run ./bin/gozerbot-udp -s to generate clientside config file "udp-send"
        * edit this file
        * test with:

        ::

            echo "YOOO" | ./bin/gozerbot-udp

    2) limiter

        on IRC the bot's /msg to a user/channel are limited to 1 per 3 seconds so the
        bot will not excessflood on the server. you can use partyudp if you need no 
        delay between sent messages, this will use dcc chat to deliver the message.
        on jabber bots there is no delay

"""


__copyright__ = 'this file is in the public domain'

# IMPORT SECTION

from gozerbot.fleet import fleet
from gozerbot.generic import rlog, handle_exception, strippedtxt, lockdec
from gozerbot.config import config
from gozerbot.plughelp import plughelp
from gozerbot.partyline import partyline
from gozerbot.threads.thr import start_new_thread
from gozerbot.contrib.rijndael import rijndael
from gozerbot.persist.persistconfig import PersistConfig

import socket, re, time, Queue

# END IMPORT

plughelp.add('udp' , 'run the udp listen thread')

# VARS SECTION

cfg = PersistConfig()
cfg.define('udp', 1) # set to 0 to disnable
cfg.define('udpparty', 0)
cfg.define('udpipv6', 0)
cfg.define('udpmasks', ['192.168*', ])
cfg.define('udphost', "localhost")
cfg.define('udpport', 5500)
cfg.define('udpallow', ["127.0.0.1", ])
cfg.define('udpallowednicks', ["#gozerbot", "dunker"])
cfg.define('udppassword', "mekker", exposed=False)
cfg.define('udpseed', "blablablablablaz", exposed=False) # needs to be 16 chars wide
cfg.define('udpstrip', 1) # strip all chars < char(32)
cfg.define('udpsleep', 0) # sleep in sendloop .. can be used to delay pack
cfg.define('udpdblog', 0)
cfg.define('udpbot', 'default')

# END VARS

def _inmask(addr):

    """ check if addr matches a mask. """

    if not cfg['udpmasks']:
        return False
    for i in cfg['udpmasks']:
        i = i.replace('*', '.*')
        if re.match(i, addr):
            return True

class Udplistener(object):

    """ 
        listen for udp messages.

    """

    def __init__(self):
        self.outqueue = Queue.Queue()
        self.queue = Queue.Queue()
        self.stop = 0
        if cfg['udpipv6']:
            self.sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        else:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        except:
            pass
        self.sock.setblocking(1)
        self.sock.settimeout(1)
        self.loggers = []

    def _outloop(self):

        """ 
            loop controling the rate of outputted messages.

            .. literalinclude:: ../../gozerplugs/udp.py
                :pyobject: Udplistener._outloop

        """

        rlog(5, 'udp', 'starting outloop')
        while not self.stop:
            (printto, txt) = self.outqueue.get()
            if self.stop:
                return
            self.dosay(printto, txt)
        rlog(5, 'udp', 'stopping outloop')

    def _handleloop(self):
        while not self.stop:
            (input, addr) = self.queue.get()
            if not input or not addr:
                continue
            if self.stop:
                break                
            self.handle(input, addr)
            if cfg['udpsleep']:
                time.sleep(cfg['udpsleep'] or 0.01)
        rlog(5, 'udp', 'shutting down udplistener')

    def _listen(self):
        """ listen for udp messages .. /msg via bot"""
        if not cfg['udp']:
            return
        try:
            fleet.startok.wait(5)
            bot = fleet.byname(cfg['udpbot'] or 'default')
            if not bot:
                rlog(10, 'udp', "can't find %s bot .. not starting" % cfg['udpbot'])
                return
            self.sock.bind((cfg['udphost'], cfg['udpport']))
            rlog(10, 'udp', 'udp listening on %s %s' % (cfg['udphost'], \
cfg['udpport']))
            self.stop = 0
        except IOError:
            handle_exception()
            self.sock = None
            self.stop = 1
            return
        # loop on listening udp socket
        bot.connectok.wait()
        while not self.stop:
            try:
                input, addr = self.sock.recvfrom(64000)
            except socket.timeout:
                continue
            except Exception, ex:
                try:
                    (errno, errstr) = ex
                except ValueError:
                    errno = 0
                    errstr = str(ex)
                if errno == 4:
                    rlog(10, self.name, str(ex))
                    break
                if errno == 35:
                    continue
                else:
                    handle_exception()
                    break
            if self.stop:
                break
            self.queue.put((input, addr))
        rlog(5, 'udp', 'shutting down main loop')

    def handle(self, input, addr):

        """ 
            handle an incoming udp packet. 

            :param input: txt in udp packet
            :type input: string
            :param addr: address info of udp packet
            :type add: (host, port) tuple

            .. literalinclude:: ../../gozerplugs/udp.py
                :pyobject: Udplistener.handle
        """

        if cfg['udpseed']:
            data = ""
            for i in range(len(input)/16):
                try:
                    data += crypt.decrypt(input[i*16:i*16+16])
                except Exception, ex:
                    rlog(10, 'udp', "can't decrypt: %s" % str(ex))
                    data = input
                    break
        else:
            data = input
        if cfg['udpstrip']:
            data = strippedtxt(data)
        # check if udp is enabled and source ip is in udpallow list
        if cfg['udp'] and (addr[0] in cfg['udpallow'] or \
_inmask(addr[0])):
            # get printto and passwd data
            header = re.search('(\S+) (\S+) (.*)', data)
            if header:
                # check password
                if header.group(1) == cfg['udppassword']:
                    printto = header.group(2)    # is the nick/channel
                    # check if printto is in allowednicks
                    if not printto in cfg['udpallowednicks']:
                        rlog(10, 'udp', "udp denied %s" % printto )
                        return
                    rlog(0, 'udp', str(addr[0]) +  " udp allowed")
                    text = header.group(3)    # is the text
                    self.say(printto, text)
                else:
                    rlog(10, 'udp', "can't match udppasswd from " + \
str(addr))
            else:
                rlog(10, 'udp', "can't match udp from " + str(addr[0]))
        else:
            rlog(10, 'udp', 'denied udp from ' + str(addr[0]))

    def say(self, printto, txt):

        """ 
            send txt to printto. 
 
            .. literalinclude:: ../../gozerplugs/udp.py
                :pyobject: Udplistener.say

        """

        self.outqueue.put((printto, txt))

    def dosay(self, printto, txt):

        """ 
            send txt to printto .. do some checks. 

           .. literalinclude:: ../../gozerplugs/udp.py
                :pyobject: Udplistener.dosay

        """

        if cfg['udpparty'] and partyline.is_on(printto):
            partyline.say_nick(printto, txt)
            return
        bot = fleet.byname(cfg['udpbot'])
        if not bot.jabber and not cfg['nolimiter']:
            time.sleep(3)
        bot.say(printto, txt)
        for i in self.loggers:
            i.log(printto, txt)

# the udplistener object
if cfg['udp']:
    udplistener = Udplistener()

# initialize crypt object if udpseed is set in config
if cfg['udp'] and cfg['udpseed']:
    crypt = rijndael(cfg['udpseed'])

def init():

    """ 
        init the udp plugin. 

        .. literalinclude:: ../../gozerplugs/udp.py
            :pyobject: init

    """

    if cfg['udp']:
        start_new_thread(udplistener._listen, ())
        start_new_thread(udplistener._handleloop, ())
        start_new_thread(udplistener._outloop, ())
    return 1
    
def shutdown():

    """ 
        shutdown the udp plugin.

        .. literalinclude:: ../../gozerplugs/udp.py
            :pyobject: init

    """

    if cfg['udp'] and udplistener:
        udplistener.stop = 1
        udplistener.outqueue.put_nowait((None, None))
        udplistener.queue.put_nowait((None, None))
    return 1

if cfg['udp'] and cfg['udpdblog']:

    from gozerbot.database.db import Db

    class Udpdblog:

        """ log udp data to database. """

        # see tables/udplog for table definition and add udpdblog = 1 to 
        # the config file
        db = Db()

        def log(self, printto, txt):

            """ do the actual logging. """

            try:
                res = self.db.execute("""INSERT into udplog(time,printto,txt)
values(%s,%s,%s) """, (time.time(), printto, txt))
            except Exception, ex:
                rlog(10, 'udp', 'failed to log to db: %s' % str(ex))
            return res

    udplistener.loggers.append(Udpdblog())
    rlog(10, 'udp', 'registered database udp logger')
